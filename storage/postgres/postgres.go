package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/storage"
)

type Store struct {
	db            *pgxpool.Pool
	product       *productRepo
	brand         *brandRepo
	categroy      *categoryRepo
	coming        *comingRepo
	comingProduct *comingProductRepo
	remainder     *remainderRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Product() storage.ProductRepoI {

	if s.product == nil {
		s.product = NewProductRepo(s.db)
	}

	return s.product

}

func (s *Store) Brand() storage.BrandRepoI {

	if s.brand == nil {
		s.brand = NewBrandRepo(s.db)
	}

	return s.brand

}

func (s *Store) Category() storage.CategoryRepoI {

	if s.categroy == nil {
		s.categroy = NewCategoryRepo(s.db)
	}

	return s.categroy

}
func (s *Store) Coming() storage.ComingRepoI {

	if s.coming == nil {
		s.coming = NewComingRepo(s.db)
	}

	return s.coming

}

func (s *Store) ComingProduct() storage.ComingProductsRepoI {

	if s.comingProduct == nil {
		s.comingProduct = NewComingProductsRepo(s.db)
	}

	return s.comingProduct

}

func (s *Store) Remainder() storage.RemainderRepoI {

	if s.remainder == nil {
		s.remainder = NewRemainderRepo(s.db)
	}

	return s.remainder

}

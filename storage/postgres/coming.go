package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type comingRepo struct {
	db *pgxpool.Pool
}

func NewComingRepo(db *pgxpool.Pool) *comingRepo {
	return &comingRepo{
		db: db,
	}
}

func (r *comingRepo) Create(ctx context.Context, req *load_service.CreateComing) (resp *load_service.ComingPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)


	query = `
		INSERT INTO "coming"(id, coming_id, branch_id, diller_id, date_time, status, created_at)
		VALUES ($1, $2, $3, $4, $5, $6, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.ComingId,
		req.BranchId,
		req.DillerId,
		time.Now().Format("2006-01-02"),
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &load_service.ComingPrimaryKey{Id: id}, nil
}

func (r *comingRepo) GetByID(ctx context.Context, req *load_service.ComingPrimaryKey) (*load_service.Coming, error) {
	fmt.Printf("%+v", req)
	var (
		query string

		id         sql.NullString
		coming_id  sql.NullString
		branch_id  sql.NullString
		diller_id  sql.NullString
		date       sql.NullString
		status     sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT
			id,
			coming_id,
			branch_id,
			diller_id,
			date_time,
			status,
			created_at
		FROM "coming"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&coming_id,
		&branch_id,
		&diller_id,
		&date,
		&status,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &load_service.Coming{
		Id:        id.String,
		ComingId:  coming_id.String,
		BranchId:  branch_id.String,
		DillerId:  diller_id.String,
		Date:      date.String,
		Status:    status.String,
		CreatedAt: created_at.String,
	}, nil
}

func (r *comingRepo) GetList(ctx context.Context, req *load_service.GetListComingRequest) (*load_service.GetListComingResponse, error) {

	var (
		resp   = &load_service.GetListComingResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			branch_id,
			diller_id,
			date_time,
			status,
			created_at,
			updated_at
		FROM "coming"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.ComingId != "" {
		where += ` AND coming_id = '` + req.ComingId + `'`
	}

	if req.BranchId != "" {
		where += ` AND branch_id = '` + req.BranchId + `'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         sql.NullString
			coming_id  sql.NullString
			branch_id  sql.NullString
			diller_id  sql.NullString
			date       sql.NullString
			status     sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&coming_id,
			&branch_id,
			&diller_id,
			&date,
			&status,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Comings = append(resp.Comings, &load_service.Coming{
			Id:        id.String,
			ComingId:  coming_id.String,
			BranchId:  branch_id.String,
			DillerId:  diller_id.String,
			Date:      date.String,
			Status:    status.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *comingRepo) Update(ctx context.Context, req *load_service.UpdateComing) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"coming"
		SET
			coming_id = :coming_id,
			branch_id = :branch_id,
			diller_id = :diller_id,
			date_time = :date_time,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"coming_id": req.GetComingId(),
		"branch_id": req.GetBranchId(),
		"diller_id": req.GetDillerId(),
		"date_time": req.GetDate(),
		"status":    req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *comingRepo) Delete(ctx context.Context, req *load_service.ComingPrimaryKey) (*load_service.ComingEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM coming WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &load_service.ComingEmpty{}, nil
}

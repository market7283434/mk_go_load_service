package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type categoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *categoryRepo {
	return &categoryRepo{
		db: db,
	}
}

func (r *categoryRepo) Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.CategoryPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "category"(id, name, parent_id, brand_id, created_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		helper.NewNullString(req.ParentId),
		req.BrandId,
	)

	if err != nil {
		return nil, err
	}

	return &load_service.CategoryPrimaryKey{Id: id}, nil
}

func (r *categoryRepo) GetByID(ctx context.Context, req *load_service.CategoryPrimaryKey) (*load_service.Category, error) {
	var (
		query string

		id         sql.NullString
		name       sql.NullString
		parent_id  sql.NullString
		brand_id   sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			parent_id,
			brand_id,
			created_at,
			updated_at
		FROM "category"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&parent_id,
		&brand_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &load_service.Category{
		Id:        id.String,
		Name:      name.String,
		ParentId:  parent_id.String,
		BrandId:   brand_id.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *categoryRepo) GetList(ctx context.Context, req *load_service.GetListCategoryRequest) (*load_service.GetListCategoryResponse, error) {

	var (
		resp   = &load_service.GetListCategoryResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			brand_id,
			created_at,
			updated_at
		FROM "category"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Name != "" {
		where += ` AND name ILIKE '%' || '` + req.Name + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			parent_id  sql.NullString
			brand_id   sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&parent_id,
			&brand_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Categories = append(resp.Categories, &load_service.Category{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parent_id.String,
			BrandId:   brand_id.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *categoryRepo) Update(ctx context.Context, req *load_service.UpdateCategory) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"category"
		SET
			name = :name,
			parent_id = :parent_id,
			brand_id = :brand_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"parent_id": helper.NewNullString(req.GetParentId()),
		"brand_id": req.GetBrandId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *categoryRepo) Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (*load_service.CategoryEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM category WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &load_service.CategoryEmpty{}, nil
}

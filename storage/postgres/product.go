package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type productRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *productRepo {
	return &productRepo{
		db: db,
	}
}

func (c *productRepo) Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.ProductPrimaryKey, err error) {
	var id = uuid.New().String()
	query := `INSERT INTO "product" (
				id,
				name,
				price,
				barcode,
				image,
				brand_id,
				category_id,
				created_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`
	_, err = c.db.Exec(ctx,
		query,
		id,
		req.Name,
		req.Price,
		req.Barcode,
		req.Image,
		req.BrandId,
		helper.NewNullString(req.CategoryId),
	)
	if err != nil {
		return nil, err
	}

	return &load_service.ProductPrimaryKey{Id: id}, nil

}

func (c *productRepo) GetByID(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error) {
	query := `
		SELECT
			id,
			name,
			price,
			barcode,
			image,
			brand_id,
			category_id,
			created_at,
			updated_at
		FROM "product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		price       sql.NullFloat64
		barcode     sql.NullString
		image       sql.NullString
		brand_id    sql.NullString
		category_id sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&price,
		&barcode,
		&image,
		&brand_id,
		&category_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.Product{
		Id:        id.String,
		Name:      name.String,
		Price:     price.Float64,
		Barcode:   barcode.String,
		Image:     image.String,
		BrandId:   brand_id.String,
		CategoryId: category_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *productRepo) GetList(ctx context.Context, req *load_service.GetListProductRequest) (resp *load_service.GetListProductResponse, err error) {

	resp = &load_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			price,
			barcode,
			image,
			brand_id,
			category_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if req.Barcode != "" {
		filter += ` AND sale_id = '` + req.Barcode + `'`
	}
	if req.Name != "" {
		filter += ` AND name ILIKE '%' || '` + req.Name + `' || '%'`
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			price       sql.NullFloat64
			barcode     sql.NullString
			image       sql.NullString
			brand_id    sql.NullString
			category_id sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&price,
			&barcode,
			&image,
			&brand_id,
			&category_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &load_service.Product{
			Id:        id.String,
			Name:      name.String,
			Price:     price.Float64,
			Barcode:   barcode.String,
			Image:     image.String,
			BrandId:   brand_id.String,
			CategoryId: category_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *productRepo) Update(ctx context.Context, req *load_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
				name = :name,
				price = :price,
				barcode = :barcode,
				image = :image,
				brand_id = :brand_id,
				category_id = :category_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"name":        req.GetName(),
		"price":       req.GetPrice(),
		"barcode":     req.GetBarcode(),
		"image":       req.GetImage(),
		"brand_id":    req.GetBrandId(),
		"category_id": req.GetCategoryId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *productRepo) Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (*load_service.ProductEmpty, error) {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &load_service.ProductEmpty{}, nil
}

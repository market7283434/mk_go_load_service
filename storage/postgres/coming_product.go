package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type comingProductRepo struct {
	db *pgxpool.Pool
}

func NewComingProductsRepo(db *pgxpool.Pool) *comingProductRepo {
	return &comingProductRepo{
		db: db,
	}
}

func (r *comingProductRepo) Create(ctx context.Context, req *load_service.CreateComingProducts) (resp *load_service.ComingProductsPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "coming_products"(id, coming_id, brand_id, category_id, name, barcode, amount, price, total_price, created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.ComingId,
		req.BrandId,
		req.CategoryId,
		req.Name,
		req.Barcode,
		req.Amount,
		req.Price,
		req.Price*float64(req.Amount),
	)

	if err != nil {
		return nil, err
	}

	return &load_service.ComingProductsPrimaryKey{Id: id}, nil
}

func (r *comingProductRepo) GetByID(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (*load_service.ComingProducts, error) {
	var (
		query string

		id          sql.NullString
		coming_id   sql.NullString
		brand_id    sql.NullString
		category_id sql.NullString
		name        sql.NullString
		barcode     sql.NullString
		amount      sql.NullInt64
		price       sql.NullFloat64
		total_price sql.NullFloat64
		created_at  sql.NullString
	)

	query = `
		SELECT
			id,
			coming_id,
			brand_id,
			category_id,
			name,
			barcode,
			amount,
			price,
			total_price,
			created_at
		FROM "coming_products"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&coming_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&amount,
		&price,
		&total_price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &load_service.ComingProducts{
		Id:         id.String,
		ComingId:   coming_id.String,
		BrandId:    brand_id.String,
		CategoryId: category_id.String,
		Name:       name.String,
		Barcode:    barcode.String,
		Amount:     amount.Int64,
		Price:      price.Float64,
		TotalPrice: total_price.Float64,
		CreatedAt:  created_at.String,
	}, nil
}

func (r *comingProductRepo) GetList(ctx context.Context, req *load_service.GetListComingProductsRequest) (*load_service.GetListComingProductsResponse, error) {

	var (
		resp   = &load_service.GetListComingProductsResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			brand_id,
			category_id,
			name,
			barcode,
			amount,
			price,
			total_price,
			created_at,
			updated_at
		FROM "coming_products"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.CategoryId != "" {
		where += ` AND category_id = '` + req.CategoryId + `'`
	}
	if req.ComingId !=""{
		where += ` AND coming_id = '` + req.ComingId + `'`
	}

	if req.BrandId != "" {
		where += ` AND brand_id = '` + req.BrandId + `'`
	}

	if req.Barcode != "" {
		where += ` AND barcode = '` + req.Barcode + `'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id          sql.NullString
			coming_id   sql.NullString
			brand_id    sql.NullString
			category_id sql.NullString
			name        sql.NullString
			barcode     sql.NullString
			amount      sql.NullInt64
			price       sql.NullFloat64
			total_price sql.NullFloat64
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&coming_id,
			&brand_id,
			&category_id,
			&name,
			&barcode,
			&amount,
			&price,
			&total_price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.ComingProducts = append(resp.ComingProducts, &load_service.ComingProducts{
			Id:         id.String,
			ComingId:   coming_id.String,
			BrandId:    brand_id.String,
			CategoryId: category_id.String,
			Name:       name.String,
			Barcode:    barcode.String,
			Amount:     amount.Int64,
			Price:      price.Float64,
			TotalPrice: total_price.Float64,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}

	return resp, nil
}

func (r *comingProductRepo) Update(ctx context.Context, req *load_service.UpdateComingProducts) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			coming_products
		SET
			coming_id = :coming_id,
			brand_id = :brand_id,
			category_id = :category_id,
			name = :name,
			barcode = :barcode,
			amount = :amount,
			price = :price,
			total_price = :total_price,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"coming_id":   req.GetComingId(),
		"brand_id":    req.GetBrandId(),
		"category_id": req.GetCategoryId(),
		"name":        req.GetName(),
		"barcode":     req.GetBarcode(),
		"amount":      req.GetAmount(),
		"price":       req.GetPrice(),
		"total_price": req.GetPrice() * float64(req.GetAmount()),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *comingProductRepo) Delete(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (*load_service.ComingProductsEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM coming_products WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &load_service.ComingProductsEmpty{}, nil
}

func (r *comingProductRepo) GetByComingID(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (*load_service.ComingProducts, error) {

	
	var (
		query string

		id          sql.NullString
		coming_id   sql.NullString
		brand_id    sql.NullString
		category_id sql.NullString
		name        sql.NullString
		barcode     sql.NullString
		amount      sql.NullInt64
		price       sql.NullFloat64
		total_price sql.NullFloat64
		created_at  sql.NullString
	)

	query = `
		SELECT
			id,
			coming_id,
			brand_id,
			category_id,
			name,
			barcode,
			amount,
			price,
			total_price,
			created_at
		FROM "coming_products"
		WHERE coming_id = $1
	`

	err := r.db.QueryRow(ctx, query, req.ComingId).Scan(
		&id,
		&coming_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&amount,
		&price,
		&total_price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}


	return &load_service.ComingProducts{
		Id:         id.String,
		ComingId:   coming_id.String,
		BrandId:    brand_id.String,
		CategoryId: category_id.String,
		Name:       name.String,
		Barcode:    barcode.String,
		Amount:     amount.Int64,
		Price:      price.Float64,
		TotalPrice: total_price.Float64,
		CreatedAt:  created_at.String,
	}, nil
}



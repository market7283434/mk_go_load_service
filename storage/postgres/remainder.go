package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type remainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) *remainderRepo {
	return &remainderRepo{
		db: db,
	}
}

func (r *remainderRepo) Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.RemainderPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "remainder"(id, branch_id, brand_id, category_id, name, barcode, amount, price, created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.BrandId,
		helper.NewNullString(req.CategoryId),
		req.Name,
		req.Barcode,
		req.Amount,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &load_service.RemainderPrimaryKey{Id: id}, nil
}

func (r *remainderRepo) GetByID(ctx context.Context, req *load_service.RemainderPrimaryKey) (*load_service.Remainder, error) {
	
	var (
		query string

		id          sql.NullString
		branch_id   sql.NullString
		brand_id    sql.NullString
		category_id sql.NullString
		name        sql.NullString
		barcode     sql.NullString
		amount      sql.NullInt64
		price       sql.NullFloat64
		created_at  sql.NullString
	)

	
	fmt.Printf("%+v", req)

	query = `
		SELECT
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			amount,
			price,
			created_at
		FROM "remainder"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&brand_id,
		&category_id,
		&name,
		&barcode,
		&amount,
		&price,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &load_service.Remainder{
		Id:         id.String,
		BranchId:   branch_id.String,
		BrandId:    brand_id.String,
		CategoryId: category_id.String,
		Name:       name.String,
		Barcode:    barcode.String,
		Amount:     amount.Int64,
		Price:      price.Float64,
		CreatedAt:  created_at.String,
	}, nil
}

func (r *remainderRepo) GetList(ctx context.Context, req *load_service.GetListRemainderRequest) (*load_service.GetListRemainderResponse, error) {

	var (
		resp   = &load_service.GetListRemainderResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			brand_id,
			category_id,
			name,
			barcode,
			amount,
			price,
			created_at,
			updated_at
		FROM remainder	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.CategoryId != "" {
		where += ` AND category_id = '` + req.CategoryId + `'`
	}

	if req.Barcode != "" {
		where += ` AND barcode = '` + req.Barcode + `'`
	}

	if req.BranchId != "" {
		where += ` AND branch_id = '` + req.BranchId + `'`
	}

	if req.BrandId != "" {
		where += ` AND brand_id = '` + req.BrandId + `'`
	}
	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id          sql.NullString
			branch_id   sql.NullString
			brand_id    sql.NullString
			category_id sql.NullString
			name        sql.NullString
			barcode     sql.NullString
			amount      sql.NullInt64
			price       sql.NullFloat64
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&brand_id,
			&category_id,
			&name,
			&barcode,
			&amount,
			&price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Remainders = append(resp.Remainders, &load_service.Remainder{
			Id:         id.String,
			BranchId:   branch_id.String,
			BrandId:    brand_id.String,
			CategoryId: category_id.String,
			Name:       name.String,
			Barcode:    barcode.String,
			Amount:     amount.Int64,
			Price:      price.Float64,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}

	return resp, nil
}

func (r *remainderRepo) Update(ctx context.Context, req *load_service.UpdateRemainder) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			remainder
		SET
			branch_id = :branch_id,
			brand_id = :brand_id,
			category_id = :category_id,
			name = :name,
			barcode = :barcode,
			amount = :amount,
			price = :price,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"brand_id":    req.GetBrandId(),
		"category_id": req.GetCategoryId(),
		"name":        req.GetName(),
		"barcode":     req.GetBarcode(),
		"amount":      req.GetAmount(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *remainderRepo) Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (*load_service.RemainderEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM remainder WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &load_service.RemainderEmpty{}, nil
}

func (r *remainderRepo) AddProduct(ctx context.Context, req *load_service.Remainder) (*load_service.RemainderPrimaryKey, error) {
	trx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, nil
	}

	defer func() {
		if err != nil {
			trx.Rollback(ctx)
		} else {
			trx.Commit(ctx)
		}
	}()

	var (
		checkQuery string
		query      string
		id         sql.NullString
		count      int64
	)

	checkQuery = `
		SELECT 
			id,
			amount
		FROM remainder
		where branch_id=$1 and barcode=$2
	`

	err = trx.QueryRow(ctx, checkQuery,
		req.BranchId,
		req.Barcode,
	).Scan(&id, &count)

	if id.String != "" {

		query = `
		UPDATE 
			remainder
		SET
			branch_id = $1,
			brand_id = $2,
			category_id = $3,
			name = $4,
			barcode = $5,
			amount = $6,
			price = $7,
			updated_at = NOW()
		WHERE id = $8 
	`

		count += req.Amount

		_, err := trx.Exec(ctx, query,
			req.BranchId,
			req.BrandId,
			req.CategoryId,
			req.Name,
			req.Barcode,
			count,
			req.Price,
			id,
		)
		if err != nil {
			return nil, err
		}

		return &load_service.RemainderPrimaryKey{Id: id.String}, nil

	} else {
		fmt.Printf("%+v", req)
		var (
			id = uuid.New().String()
		)
		query = `
		INSERT INTO remainder(id, branch_id, brand_id, category_id, name, barcode, amount, price, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`
		_, err = trx.Exec(ctx, query,
			id,
			req.BranchId,
			req.BrandId,
			helper.NewNullString(req.CategoryId),
			req.Name,
			req.Barcode,
			req.Amount,
			req.Price,
		)

		if err != nil {
			return nil, err
		}

		return &load_service.RemainderPrimaryKey{Id: id}, nil

	}

	
}

func (r *remainderRepo) DeleteProduct(ctx context.Context, req *load_service.Remainder) (*load_service.RemainderPrimaryKey, error) {
	
	trx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, nil
	}


	defer func() {
		if err != nil {
			trx.Rollback(ctx)
		} else {
			trx.Commit(ctx)
		}
	}()


	var (
		checkQuery string
		query      string
		id         sql.NullString
		count      int64
	)

	checkQuery = `
		SELECT 
			id,
			amount
		FROM remainder
		where branch_id=$1 and barcode=$2
	`

	err = trx.QueryRow(ctx, checkQuery,
		req.BranchId,
		req.Barcode,
	).Scan(&id, &count)

	query = `
		UPDATE 
			remainder
		SET
			amount = $1,
			updated_at = NOW()
		WHERE id = $2 
	`

	count -= req.Amount

	_, err = trx.Exec(ctx, query,
		count,
		id,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.RemainderPrimaryKey{Id: id.String}, nil

}

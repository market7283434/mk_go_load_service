package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/pkg/helper"
)

type brandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *brandRepo {
	return &brandRepo{
		db: db,
	}
}

func (r *brandRepo) Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.BrandPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "brand"(id, name, image, created_at)
		VALUES ($1, $2, $3, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Image,
	)

	if err != nil {
		return nil, err
	}

	return &load_service.BrandPrimaryKey{Id: id}, nil
}

func (r *brandRepo) GetByID(ctx context.Context, req *load_service.BrandPrimaryKey) (*load_service.Brand, error) {
	var (
		query string

		id         sql.NullString
		name       sql.NullString
		image      sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			image,
			created_at,
			updated_at
		FROM "brand"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&image,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &load_service.Brand{
		Id:        id.String,
		Name:      name.String,
		Image:     image.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *brandRepo) GetList(ctx context.Context, req *load_service.GetListBrandRequest) (*load_service.GetListBrandResponse, error) {

	var (
		resp   = &load_service.GetListBrandResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			image,
			created_at,
			updated_at
		FROM "brand"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Name != "" {
		where += ` AND name ILIKE '%' || '` + req.Name + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			image      sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&image,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Brands = append(resp.Brands, &load_service.Brand{
			Id:        id.String,
			Name:      name.String,
			Image:     image.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *brandRepo) Update(ctx context.Context, req *load_service.UpdateBrand) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"brand"
		SET
			name = :name,
			image = :image,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":    req.Id,
		"name":  req.Name,
		"image": req.Image,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *brandRepo) Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (*load_service.BrandEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM brand WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &load_service.BrandEmpty{}, nil
}

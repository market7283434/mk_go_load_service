package storage

import (
	"context"

	"gitlab.com/market/mk_go_load_service/genproto/load_service"
)

type StorageI interface {
	CloseDB()
	Product() ProductRepoI
	Brand() BrandRepoI
	Category() CategoryRepoI
	Coming() ComingRepoI
	ComingProduct() ComingProductsRepoI
	Remainder() RemainderRepoI
}

type ProductRepoI interface {
	Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.ProductPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error)
	GetList(ctx context.Context, req *load_service.GetListProductRequest) (resp *load_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (*load_service.ProductEmpty, error)
}

type BrandRepoI interface {
	Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.BrandPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.Brand, err error)
	GetList(ctx context.Context, req *load_service.GetListBrandRequest) (resp *load_service.GetListBrandResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateBrand) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (*load_service.BrandEmpty, error)
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.CategoryPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.Category, err error)
	GetList(ctx context.Context, req *load_service.GetListCategoryRequest) (resp *load_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateCategory) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (*load_service.CategoryEmpty, error)
}
type ComingRepoI interface {
	Create(ctx context.Context, req *load_service.CreateComing) (resp *load_service.ComingPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.ComingPrimaryKey) (resp *load_service.Coming, err error)
	GetList(ctx context.Context, req *load_service.GetListComingRequest) (resp *load_service.GetListComingResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateComing) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ComingPrimaryKey) (*load_service.ComingEmpty, error)
}

type ComingProductsRepoI interface {
	Create(ctx context.Context, req *load_service.CreateComingProducts) (resp *load_service.ComingProductsPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (resp *load_service.ComingProducts, err error)
	GetList(ctx context.Context, req *load_service.GetListComingProductsRequest) (resp *load_service.GetListComingProductsResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateComingProducts) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (*load_service.ComingProductsEmpty, error)
}

type RemainderRepoI interface {
	Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.RemainderPrimaryKey, err error)
	GetByID(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.Remainder, err error)
	GetList(ctx context.Context, req *load_service.GetListRemainderRequest) (resp *load_service.GetListRemainderResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateRemainder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (*load_service.RemainderEmpty, error)
	AddProduct(ctx context.Context, req *load_service.Remainder) (*load_service.RemainderPrimaryKey, error)
	DeleteProduct(ctx context.Context, req *load_service.Remainder) (*load_service.RemainderPrimaryKey, error)
}

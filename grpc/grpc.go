package grpc

import (
	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/grpc/service"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	load_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	load_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	load_service.RegisterBrandServiceServer(grpcServer, service.NewBrandService(cfg, log, strg, srvc))
	load_service.RegisterComingServiceServer(grpcServer, service.NewComingService(cfg, log, strg, srvc))
	load_service.RegisterComingProductsServiceServer(grpcServer, service.NewComingProductsService(cfg, log, strg, srvc))
	load_service.RegisterRemainderServiceServer(grpcServer, service.NewRemainderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}

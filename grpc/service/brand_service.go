package service

import (
	"context"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BrandService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BrandService {
	return &BrandService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *BrandService) Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.Brand, err error) {
	i.log.Info("---------CreateBrand---------", logger.Any("req", req))

	id, err := i.strg.Brand().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBrand->Brand->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Brand().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetBrandById --> Create Response->Brand", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *BrandService) GetByID(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.Brand, err error) {
	i.log.Info("---------GetBrandById---------", logger.Any("req", req))

	resp, err = i.strg.Brand().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBrandById->Brand->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *BrandService) GetList(ctx context.Context, req *load_service.GetListBrandRequest) (resp *load_service.GetListBrandResponse, err error) {

	i.log.Info("---GetBrands------>", logger.Any("req", req))

	resp, err = i.strg.Brand().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBrands->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BrandService) Update(ctx context.Context, req *load_service.UpdateBrand) (resp *load_service.Brand, err error) {

	i.log.Info("---UpdateBrand------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Brand().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBrand--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Brand().GetByID(ctx, &load_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BrandService) Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.BrandEmpty, err error) {

	i.log.Info("---DeleteBrand------>", logger.Any("req", req))

	resp, err = i.strg.Brand().Delete(ctx, &load_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

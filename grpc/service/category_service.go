package service

import (
	"context"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *CategoryService) Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.Category, err error) {
	i.log.Info("---------CreateCategory---------", logger.Any("req", req))

	id, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCategory->Category->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Category().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetCategoryById --> Create Response->Category", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *CategoryService) GetByID(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.Category, err error) {
	i.log.Info("---------GetCategoryById---------", logger.Any("req", req))

	resp, err = i.strg.Category().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategoryById->Category->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *CategoryService) GetList(ctx context.Context, req *load_service.GetListCategoryRequest) (resp *load_service.GetListCategoryResponse, err error) {

	i.log.Info("---GetCategorys------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategorys->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) Update(ctx context.Context, req *load_service.UpdateCategory) (resp *load_service.Category, err error) {

	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Category().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByID(ctx, &load_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CategoryService) Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.CategoryEmpty, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	resp, err = i.strg.Category().Delete(ctx, &load_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

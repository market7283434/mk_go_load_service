package service

import (
	"context"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ComingProductsService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedComingProductsServiceServer
}

func NewComingProductsService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ComingProductsService {
	return &ComingProductsService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *ComingProductsService) Create(ctx context.Context, req *load_service.CreateComingProducts) (resp *load_service.ComingProducts, err error) {
	i.log.Info("---------CreateComingProducts---------", logger.Any("req", req))

	id, err := i.strg.ComingProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateComingProducts->ComingProducts->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ComingProduct().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetComingProductsById --> Create Response->ComingProducts", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ComingProductsService) GetByID(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (resp *load_service.ComingProducts, err error) {
	i.log.Info("---------GetComingProductsById---------", logger.Any("req", req))

	resp, err = i.strg.ComingProduct().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingProductsById->ComingProducts->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ComingProductsService) GetList(ctx context.Context, req *load_service.GetListComingProductsRequest) (resp *load_service.GetListComingProductsResponse, err error) {

	i.log.Info("---GetComingProductss------>", logger.Any("req", req))

	resp, err = i.strg.ComingProduct().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingProductss->ComingProducts->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingProductsService) Update(ctx context.Context, req *load_service.UpdateComingProducts) (resp *load_service.ComingProducts, err error) {

	i.log.Info("---UpdateComingProducts------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ComingProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateComingProducts--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ComingProduct().GetByID(ctx, &load_service.ComingProductsPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComingProducts->ComingProducts->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ComingProductsService) Delete(ctx context.Context, req *load_service.ComingProductsPrimaryKey) (resp *load_service.ComingProductsEmpty, err error) {

	i.log.Info("---DeleteComingProducts------>", logger.Any("req", req))

	resp, err = i.strg.ComingProduct().Delete(ctx, &load_service.ComingProductsPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteComingProducts->ComingProducts->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

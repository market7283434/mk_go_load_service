package service

import (
	"context"
	"fmt"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type RemainderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedRemainderServiceServer
}

func NewRemainderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *RemainderService {
	return &RemainderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *RemainderService) Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.Remainder, err error) {
	i.log.Info("---------CreateRemainder---------", logger.Any("req", req))

	id, err := i.strg.Remainder().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateRemainder->Remainder->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Remainder().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetRemainderById --> Create Response->Remainder", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *RemainderService) GetByID(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.Remainder, err error) {
	i.log.Info("---------GetRemainderById---------", logger.Any("req", req))

	resp, err = i.strg.Remainder().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRemainderById->Remainder->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *RemainderService) GetList(ctx context.Context, req *load_service.GetListRemainderRequest) (resp *load_service.GetListRemainderResponse, err error) {

	i.log.Info("---GetRemainders------>", logger.Any("req", req))

	resp, err = i.strg.Remainder().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRemainders->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RemainderService) Update(ctx context.Context, req *load_service.UpdateRemainder) (resp *load_service.Remainder, err error) {

	i.log.Info("---UpdateRemainder------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Remainder().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateRemainder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Remainder().GetByID(ctx, &load_service.RemainderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetRemainder->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *RemainderService) Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.RemainderEmpty, err error) {

	i.log.Info("---DeleteRemainder------>", logger.Any("req", req))

	resp, err = i.strg.Remainder().Delete(ctx, &load_service.RemainderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteRemainder->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
func (i *RemainderService) AddProduct(ctx context.Context, req *load_service.Remainder)(resp *load_service.Remainder, err error){
	i.log.Info("---Add Product to Remainder------>", logger.Any("req", req))

	id, err := i.strg.Remainder().AddProduct(ctx, req)

	if err != nil {
		i.log.Error("!!!Add Product to Remainder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if id == nil {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Remainder().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetRemainder->Add Product to Remainder--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *RemainderService) DeleteProduct(ctx context.Context, req *load_service.Remainder)(resp *load_service.Remainder, err error){
	i.log.Info("---Delete Product to Remainder------>", logger.Any("req", req))


	id, err := i.strg.Remainder().DeleteProduct(ctx, req)

	if err != nil {
		i.log.Error("!!!Delete Product to Remainder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if id == nil {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	fmt.Printf("%+v",id)
	fmt.Println()
	resp, err = i.strg.Remainder().GetByID(ctx, &load_service.RemainderPrimaryKey{Id: id.Id})
	if err != nil {
		i.log.Error("!!!GetRemainder->Delete Product to Remainder--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}


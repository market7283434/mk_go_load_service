package service

import (
	"context"
	"fmt"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ComingService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedComingServiceServer
}

func NewComingService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ComingService {
	return &ComingService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *ComingService) Create(ctx context.Context, req *load_service.CreateComing) (resp *load_service.Coming, err error) {
	i.log.Info("---------CreateComing---------", logger.Any("req", req))

	id, err := i.strg.Coming().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateComing->Coming->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Coming().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetComingById --> Create Response->Coming", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ComingService) GetByID(ctx context.Context, req *load_service.ComingPrimaryKey) (resp *load_service.Coming, err error) {
	i.log.Info("---------GetComingById---------", logger.Any("req", req))

	fmt.Println(req)
	resp, err = i.strg.Coming().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingById->Coming->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ComingService) GetList(ctx context.Context, req *load_service.GetListComingRequest) (resp *load_service.GetListComingResponse, err error) {

	i.log.Info("---GetComings------>", logger.Any("req", req))

	resp, err = i.strg.Coming().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComings->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingService) Update(ctx context.Context, req *load_service.UpdateComing) (resp *load_service.Coming, err error) {

	i.log.Info("---UpdateComing------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Coming().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateComing--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Coming().GetByID(ctx, &load_service.ComingPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	

	return resp, err
}

func (i *ComingService) Delete(ctx context.Context, req *load_service.ComingPrimaryKey) (resp *load_service.ComingEmpty, err error) {

	i.log.Info("---DeleteComing------>", logger.Any("req", req))

	resp, err = i.strg.Coming().Delete(ctx, &load_service.ComingPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

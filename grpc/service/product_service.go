package service

import (
	"context"

	"gitlab.com/market/mk_go_load_service/config"
	"gitlab.com/market/mk_go_load_service/genproto/load_service"
	"gitlab.com/market/mk_go_load_service/grpc/client"
	"gitlab.com/market/mk_go_load_service/pkg/logger"
	"gitlab.com/market/mk_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*load_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *ProductService) Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.Product, err error) {
	i.log.Info("---------CreateProduct---------", logger.Any("req", req))

	id, err := i.strg.Product().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Product->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Product().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetProductById --> Create Response->Product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ProductService) GetByID(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error) {
	i.log.Info("---------GetProductById---------", logger.Any("req", req))

	resp, err = i.strg.Product().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProductById->Product->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ProductService) GetByBarcode(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error) {
	i.log.Info("---------GetProductByBarcode---------", logger.Any("req", req))

	resp, err = i.strg.Product().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProductByBarcode->Product->GetByBarcode", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ProductService) GetList(ctx context.Context, req *load_service.GetListProductRequest) (resp *load_service.GetListProductResponse, err error) {

	i.log.Info("---GetProducts------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProducts->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) Update(ctx context.Context, req *load_service.UpdateProduct) (resp *load_service.Product, err error) {

	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByID(ctx, &load_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.ProductEmpty, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	resp, err = i.strg.Product().Delete(ctx, &load_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

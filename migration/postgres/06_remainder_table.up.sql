CREATE TABLE "remainder" (
    "id" UUID NOT NULL PRIMARY KEY,
    "branch_id" UUID NOT NULL,
    "brand_id" UUID NOT NULL,
    "category_id" UUID REFERENCES "category"("id"),
    "name" VARCHAR NOT NULL,
    "barcode" VARCHAR NOT NULL,
    "amount"   INT NOT NULL,
    "price"  NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


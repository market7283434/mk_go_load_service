
CREATE TABLE "category" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "parent_id" UUID REFERENCES "category"("id"),
    "brand_id" UUID REFERENCES "brand"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
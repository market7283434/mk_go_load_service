
CREATE TABLE "brand" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "image" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);